package alirezat775.app.huaweiMap

import alirezat775.app.huaweiMap.helper.PermissionRequest
import android.Manifest
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class MapActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        permissionRequest().send()

        goToGoogle.setOnClickListener {
            val myIntent = Intent(this@MapActivity, MapGoogleActivity::class.java)
            this@MapActivity.startActivity(myIntent)
        }
        goToHuawei.setOnClickListener {
            val myIntent = Intent(this@MapActivity, MapHuaweiActivity::class.java)
            this@MapActivity.startActivity(myIntent)
        }
    }

    private fun permissionRequest(): PermissionRequest {
        return PermissionRequest(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
    }
}