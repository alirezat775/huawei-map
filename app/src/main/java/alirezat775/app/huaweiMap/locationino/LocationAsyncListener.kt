package alirezat775.app.huaweiMap.locationino

import android.location.Location

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

interface LocationAsyncListener {

    fun onSuccess(location: Location?)
    fun onFailure(exception: Exception)
}