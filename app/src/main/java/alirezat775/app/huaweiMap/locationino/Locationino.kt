package alirezat775.app.huaweiMap.locationino

import android.annotation.SuppressLint
import android.location.Location

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class Locationino(
    private val huaweiFusedLocationProviderClient: com.huawei.hms.location.FusedLocationProviderClient? = null,
    private val googleFusedLocationProviderClient: com.google.android.gms.location.FusedLocationProviderClient? = null
) {

    private fun isGoogle(): Boolean {
        return googleFusedLocationProviderClient != null
    }

    private fun isHuawei(): Boolean {
        return huaweiFusedLocationProviderClient != null
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation(listener: LocationAsyncListener) {
        if (isGoogle()) {
            try {
                val lastLocation: com.google.android.gms.tasks.Task<Location> =
                    googleFusedLocationProviderClient?.lastLocation as com.google.android.gms.tasks.Task<Location>
                lastLocation.addOnSuccessListener(com.google.android.gms.tasks.OnSuccessListener { location ->
                    listener.onSuccess(location)
                    return@OnSuccessListener
                }).addOnFailureListener {
                    listener.onFailure(it)
                }
            } catch (e: Exception) {
                listener.onFailure(e)
            }
        }
        if (isHuawei()) {
            try {
                val lastLocation: com.huawei.hmf.tasks.Task<Location> =
                    huaweiFusedLocationProviderClient?.lastLocation as com.huawei.hmf.tasks.Task<Location>
                lastLocation.addOnSuccessListener(com.huawei.hmf.tasks.OnSuccessListener { location ->
                    listener.onSuccess(location)
                    return@OnSuccessListener
                }).addOnFailureListener {
                    listener.onFailure(it)
                }
            } catch (e: Exception) {
                listener.onFailure(e)
            }
        }
    }
}