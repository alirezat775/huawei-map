package alirezat775.app.huaweiMap

import alirezat775.app.huaweiMap.helper.LocationManager
import alirezat775.app.huaweiMap.helper.PermissionRequest
import alirezat775.app.huaweiMap.locationino.LocationAsyncListener
import alirezat775.app.huaweiMap.locationino.Locationino
import alirezat775.app.huaweiMap.mapino.MapAsyncListener
import alirezat775.app.huaweiMap.mapino.MapManager
import alirezat775.app.huaweiMap.mapino.Mapino
import alirezat775.app.huaweiMap.mapino.wrapper.BitmapDescriptorFactory
import alirezat775.app.huaweiMap.mapino.wrapper.CameraUpdate
import alirezat775.app.huaweiMap.mapino.wrapper.LatLng
import alirezat775.app.huaweiMap.mapino.wrapper.MarkerOptions
import android.Manifest
import android.location.Location
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import kotlinx.android.synthetic.main.activity_google.*

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class MapGoogleActivity : AppCompatActivity(), MapAsyncListener,
    Mapino.OnMyLocationButtonClickListener,
    Mapino.OnMapClickListener {

    private lateinit var mapino: Mapino
    private val MAP_STATE = "MapViewState"
    private val tehranLatLng = LatLng(35.6892, 51.3890)
    private lateinit var map: MapManager
    private lateinit var locationino: Locationino

    private val locationManager = LocationManager(this)

    private var lastLocation = LatLng(0.0, 0.0)

    private val DEFUALT_ZOOM_IN = 20f
    private val DEFUALT_ZOOM_MEDIUM_IN = 15f
    private val DEFUALT_ZOOM_OUT = 10f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google)

        //init map
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_STATE)
        }
        map = MapManager(mapView)
        map.onCreateMap(mapViewBundle)
        map.getMapAsync(this)

        //init location
        if (permissionRequest().isGranted()) {
            locationino = Locationino(
                googleFusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(this)
            )
            getLastLocation()
        } else {
            permissionRequest().send()
        }

        pinMarker()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    private fun permissionRequest(): PermissionRequest {
        return PermissionRequest(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
    }

    override fun onMapReady(mapino: Mapino) {
        this.mapino = mapino
        with(mapino) {
            if (permissionRequest().isGranted()) {
                setMyLocationEnabled(true)
            }
            animateCamera(
                CameraUpdate(
                    google = CameraUpdateFactory.newLatLngZoom(
                        com.google.android.gms.maps.model.LatLng(
                            tehranLatLng.google.latitude, tehranLatLng.google.longitude
                        ), DEFUALT_ZOOM_OUT
                    )
                )
            )
            setMapType(MAP_TYPE_NORMAL)
            mapino.setOnMyLocationButtonClickListener(this@MapGoogleActivity)
            mapino.setOnMapClickListener(this@MapGoogleActivity)
        }
    }

    private fun pinMarker() {
        pinMarker.setOnClickListener {
            val midLatLng: LatLng = mapino.getCameraPosition()?.target!!
            pinAddress.text =
                locationManager.getAddress(midLatLng.huawei.latitude, midLatLng.huawei.longitude)
        }
    }

    override fun onMyLocationButtonClick(): Boolean {
        mapino.animateCamera(
            CameraUpdate(
                google = CameraUpdateFactory.newLatLngZoom(
                    com.google.android.gms.maps.model.LatLng(
                        lastLocation.google.latitude, lastLocation.google.longitude
                    ), DEFUALT_ZOOM_IN
                )
            )
        )
        return true
    }

    private fun getLastLocation() {
        locationino.getLastLocation(object : LocationAsyncListener {
            override fun onSuccess(location: Location?) {
                lastLocation = LatLng(location?.latitude ?: 0.0, location?.longitude ?: 0.0)
            }

            override fun onFailure(exception: Exception) {
            }
        })
    }

    override fun onMapClick(position: LatLng?) {
        val markerOptions = MarkerOptions()
        markerOptions.position(position!!).title(
            locationManager.getAddress(
                position.google.latitude,
                position.google.longitude
            )
        ).icon(BitmapDescriptorFactory.fromResource(R.drawable.star))
        mapino.clear()
        mapino.animateCamera(
            CameraUpdate(
                google = CameraUpdateFactory.newLatLngZoom(
                    com.google.android.gms.maps.model.LatLng(
                        position.google.latitude, position.google.longitude
                    ), DEFUALT_ZOOM_MEDIUM_IN
                )
            )
        )
        mapino.addMarker(markerOptions)
    }
}