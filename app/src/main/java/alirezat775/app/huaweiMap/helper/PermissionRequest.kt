package alirezat775.app.huaweiMap.helper

import android.content.Context
import android.content.pm.PackageManager
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import kotlin.math.roundToInt

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class PermissionRequest(
    @NonNull activity: AppCompatActivity?,
    @NonNull vararg permissions: String
) {
    private val request = Request()

    init {
        request.permissions = permissions
        request.activity = activity
        request.code = (Math.random() * 10000).roundToInt()
    }

    fun isGranted(): Boolean {
        for (p in request.permissions) {
            if (ActivityCompat.checkSelfPermission(
                    request.context,
                    p
                ) != PackageManager.PERMISSION_GRANTED
            ) return false
        }
        return true
    }

    @JvmOverloads
    fun send(@NonNull repeatCount: Int = 1) {
        request.send(repeatCount - 1)
    }

    fun setOnGrantListener(@NonNull onGrant: Runnable?) {
        request.onGrant = onGrant
    }

    fun setOnNotGrantListener(@NonNull onNotGrant: Runnable?) {
        request.onNotGrant = onNotGrant
    }

    private class Request {
        var code = 0
        var activity: AppCompatActivity? = null
        var permissions: Array<out String> = emptyArray()
        var onGrant: Runnable? = null
        var onNotGrant: Runnable? = null
        var repeat = 0

        fun send(repeatCount: Int) {
            var notGranted: MutableList<String?>? = null
            for (p in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        p
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (notGranted == null) notGranted = ArrayList()
                    notGranted.add(p)
                }
            }
            if (notGranted == null) return
            repeat = repeatCount
            if (!requests.containsKey(code)) requests[code] = this
            ActivityCompat.requestPermissions(activity!!, notGranted.toTypedArray(), code)
        }

        fun resend() {
            send(repeat - 1)
        }

        fun granted() {
            if (!requests.containsKey(code)) requests.remove(code)
            onGrant?.run()
        }

        fun notGranted() {
            if (!requests.containsKey(code)) requests.remove(code)
            onNotGrant?.run()
        }

        val context: Context
            get() = (activity as Context)
    }

    companion object {
        private val requests: MutableMap<Int, Request> = HashMap()

        fun onRequestPermissionsResult(
            @NonNull requestCode: Int,
            @NonNull permissions: Array<String?>?,
            @NonNull grantResults: IntArray
        ) {
            if (requests.containsKey(requestCode)) {
                if (isGrantedRequests(grantResults)) {
                    requests[requestCode]!!.granted()
                } else {
                    if (requests[requestCode]!!.repeat > 0) {
                        requests[requestCode]!!.resend()
                    } else {
                        requests[requestCode]!!.notGranted()
                    }
                }
            }
        }

        private fun isGrantedRequests(@NonNull grantResults: IntArray): Boolean {
            for (g in grantResults) {
                if (g != PermissionChecker.PERMISSION_GRANTED) return false
            }
            return true
        }
    }
}