package alirezat775.app.huaweiMap.helper

import android.Manifest
import android.annotation.SuppressLint
import android.location.Geocoder
import android.location.LocationListener
import android.location.LocationManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import java.util.*

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class LocationManager(private val context: AppCompatActivity) {

    fun getAddress(latitude: Double, longitude: Double): String {
        return try {
            val geocode = Geocoder(context, Locale.getDefault())
            val addressLocation = geocode.getFromLocation(latitude, longitude, 1)
            addressLocation.first().getAddressLine(0)
        } catch (e: Exception) {
            Log.e(this::class.simpleName, e.message, e)
            ""
        }
    }

    @SuppressLint("MissingPermission")
    fun getCurrentLocation(locationListener: LocationListener) {
        if (permissionRequest().isGranted()) {
            val manager =
                context.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager
            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5L, 20F, locationListener)
        } else {
            permissionRequest().send()
        }
    }

    private fun permissionRequest(): PermissionRequest {
        return PermissionRequest(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
    }
}