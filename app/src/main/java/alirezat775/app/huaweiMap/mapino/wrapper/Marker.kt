package alirezat775.app.huaweiMap.mapino.wrapper

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class Marker(
    var google: com.google.android.gms.maps.model.Marker?,
    var huawei: com.huawei.hms.maps.model.Marker?
) {
    fun setPosition(latLng: LatLng) {
        if (google != null) google!!.position = latLng.google
        if (huawei != null) huawei!!.position = latLng.huawei
    }

    fun getPosition(): LatLng? {
        var latLng: LatLng? = null
        if (google != null) latLng = LatLng(google!!.position.latitude, google!!.position.longitude)
        if (huawei != null) latLng = LatLng(huawei!!.position.latitude, huawei!!.position.longitude)
        return latLng
    }

    fun setIcon(value: BitmapDescriptor?) {
        if (google != null) google!!.setIcon(value?.google)
        if (huawei != null) huawei!!.setIcon(value?.huawei)
    }

    fun setAnchor(x: Float, y: Float) {
        if (google != null) google!!.setAnchor(x, y)
        if (huawei != null) huawei!!.setMarkerAnchor(x, y)
    }

    fun setTitle(value: String?) {
        if (google != null) google!!.title = value
        if (huawei != null) huawei!!.title = value
    }

    fun getTitle(): String? {
        if (google != null) return google!!.title
        return if (huawei != null) huawei!!.title else null
    }

    fun setSnippet(value: String?) {
        if (google != null) google!!.snippet = value
        if (huawei != null) huawei!!.snippet = value
    }

    fun getSnippet(): String? {
        var res: String? = null
        if (google != null) res = google!!.snippet
        if (huawei != null) res = huawei!!.snippet
        return res
    }

    fun isInfoWindowShown(): Boolean {
        var res = false
        if (google != null) res = google!!.isInfoWindowShown
        if (huawei != null) res = huawei!!.isInfoWindowShown
        return res
    }

    fun setFlat(value: Boolean) {
        if (google != null) google!!.isFlat = value
        if (huawei != null) huawei!!.isFlat = value
    }

    fun isFlat(): Boolean {
        var res = false
        if (google != null) res = google!!.isFlat
        if (huawei != null) res = huawei!!.isFlat
        return res
    }

    fun setRotation(value: Float) {
        if (google != null) google!!.rotation = value
        if (huawei != null) huawei!!.rotation = value
    }

    fun getRotation(): Float {
        var res = -1f
        if (google != null) res = google!!.rotation
        if (huawei != null) res = huawei!!.rotation
        return res
    }

    fun setZIndex(value: Float) {
        if (google != null) google!!.zIndex = value
        if (huawei != null) huawei!!.zIndex = value
    }

    fun getZIndex(): Float {
        var value = -1f
        if (google != null) value = google!!.zIndex
        if (huawei != null) value = huawei!!.zIndex
        return value
    }

    fun setAlpha(value: Float) {
        if (google != null) google!!.alpha = value
        if (huawei != null) huawei!!.alpha = value
    }

    fun getAlpha(): Float {
        var res = -1f
        if (google != null) res = google!!.alpha
        if (huawei != null) res = huawei!!.alpha
        return res
    }

    fun setTag(value: Any?) {
        if (google != null) google!!.tag = value
        if (huawei != null) huawei!!.tag = value
    }

    fun getTag(): Any? {
        var res: Any? = null
        if (google != null) res = google!!.tag
        if (huawei != null) res = huawei!!.tag
        return res
    }

    fun setVisible(value: Boolean) {
        if (google != null) google!!.isVisible = value
        if (huawei != null) huawei!!.isVisible = value
    }

    fun showInfoWindow() {
        if (google != null) google!!.showInfoWindow()
        if (huawei != null) huawei!!.showInfoWindow()
    }

}