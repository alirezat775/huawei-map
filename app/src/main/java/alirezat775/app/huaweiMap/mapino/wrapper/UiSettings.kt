package alirezat775.app.huaweiMap.mapino.wrapper

import com.google.android.gms.maps.GoogleMap

import com.huawei.hms.maps.HuaweiMap


/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */


class UiSettings {
    private var huaweiMap: HuaweiMap? = null
    private var googleMap: GoogleMap? = null

    constructor(huaweiMap: HuaweiMap?) {
        this.huaweiMap = huaweiMap
    }

    constructor(googleMap: GoogleMap?) {
        this.googleMap = googleMap
    }

    fun setZoomControlsEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isZoomControlsEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isZoomControlsEnabled = value
    }

    fun setCompassEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isCompassEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isCompassEnabled = value
    }

    fun setMyLocationButtonEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isMyLocationButtonEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isMyLocationButtonEnabled = value
    }

    fun setIndoorLevelPickerEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isIndoorLevelPickerEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isIndoorLevelPickerEnabled = value
    }

    fun setScrollGesturesEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isScrollGesturesEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isScrollGesturesEnabled = value
    }

    fun setZoomGesturesEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isZoomGesturesEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isZoomGesturesEnabled = value
    }

    fun setTiltGesturesEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isTiltGesturesEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isTiltGesturesEnabled = value
    }

    fun setRotateGesturesEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isRotateGesturesEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isRotateGesturesEnabled = value
    }

    fun setScrollGesturesEnabledDuringRotateOrZoom(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isScrollGesturesEnabledDuringRotateOrZoom =
            value
        if (googleMap != null) googleMap!!.uiSettings.setScrollGesturesEnabledDuringRotateOrZoom(
            value
        )
    }

    fun setAllGesturesEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.setAllGesturesEnabled(value)
        if (googleMap != null) googleMap!!.uiSettings.setAllGesturesEnabled(value)
    }

    fun setMapToolbarEnabled(value: Boolean) {
        if (huaweiMap != null) huaweiMap!!.uiSettings.isMapToolbarEnabled = value
        if (googleMap != null) googleMap!!.uiSettings.isMapToolbarEnabled = value
    }

    fun isZoomControlsEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isZoomControlsEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isZoomControlsEnabled else false
    }

    fun isCompassEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isCompassEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isCompassEnabled else false
    }

    fun isMyLocationButtonEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isMyLocationButtonEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isMyLocationButtonEnabled else false
    }

    fun isIndoorLevelPickerEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isIndoorLevelPickerEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isIndoorLevelPickerEnabled else false
    }

    fun isScrollGesturesEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isScrollGesturesEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isScrollGesturesEnabled else false
    }

    fun isScrollGesturesEnabledDuringRotateOrZoom(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isScrollGesturesEnabledDuringRotateOrZoom
        return if (googleMap != null) googleMap!!.uiSettings.isScrollGesturesEnabledDuringRotateOrZoom else false
    }

    fun isZoomGesturesEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isZoomGesturesEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isZoomGesturesEnabled else false
    }

    fun isTiltGesturesEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isTiltGesturesEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isTiltGesturesEnabled else false
    }

    fun isRotateGesturesEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isRotateGesturesEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isRotateGesturesEnabled else false
    }

    fun isMapToolbarEnabled(): Boolean {
        if (huaweiMap != null) return huaweiMap!!.uiSettings.isMapToolbarEnabled
        return if (googleMap != null) googleMap!!.uiSettings.isMapToolbarEnabled else false
    }
}