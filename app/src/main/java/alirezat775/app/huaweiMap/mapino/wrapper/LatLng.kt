package alirezat775.app.huaweiMap.mapino.wrapper


/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class LatLng(latitude: Double, longitude: Double) {
    var huawei = com.huawei.hms.maps.model.LatLng(latitude, longitude)
    var google = com.google.android.gms.maps.model.LatLng(latitude, longitude)
}