package alirezat775.app.huaweiMap.mapino

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

interface MapAsyncListener {

    fun onMapReady(mapino: Mapino)
}