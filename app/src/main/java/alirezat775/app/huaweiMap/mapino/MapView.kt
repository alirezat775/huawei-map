package alirezat775.app.huaweiMap.mapino

import alirezat775.app.huaweiMap.R
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.widget.FrameLayout

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

@SuppressLint("ViewConstructor")
class MapView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private lateinit var mapManagerTypeModel: MapManager.MapTypeModel

    init {
        applyType(context, attrs)
    }

    private fun applyType(context: Context, attrs: AttributeSet?) {
        val attr = context.theme.obtainStyledAttributes(attrs, R.styleable.MapViewAttr, 0, 0)
        val pureMapTypeModel = try {
            attr.getInteger(R.styleable.MapViewAttr_mapTypeModel, 0)
        } finally {
            attr.recycle()
        }
        detectMapTypeModel(pureMapTypeModel)
    }

    private fun detectMapTypeModel(type: Int) {
        mapManagerTypeModel = when (type) {
            0 -> {
                MapManager.MapTypeModel.HUAWEI
            }
            1 -> {
                MapManager.MapTypeModel.GOOGLE
            }
            else -> MapManager.MapTypeModel.HUAWEI
        }
    }

    private var map: FrameLayout = when (mapManagerTypeModel) {
        MapManager.MapTypeModel.HUAWEI -> {
            com.huawei.hms.maps.MapView(context)
        }
        MapManager.MapTypeModel.GOOGLE -> {
            com.google.android.gms.maps.MapView(context)
        }
    }

    fun getMap(): FrameLayout {
        return map
    }

    fun getMapTypeModel(): MapManager.MapTypeModel {
        return mapManagerTypeModel
    }

    fun onStart() {
        when (getMapTypeModel()) {
            MapManager.MapTypeModel.HUAWEI -> {
                (map as com.huawei.hms.maps.MapView).onStart()
            }
            MapManager.MapTypeModel.GOOGLE -> {
                (map as com.google.android.gms.maps.MapView).onStart()
            }
        }
    }

    fun onStop() {
        when (getMapTypeModel()) {
            MapManager.MapTypeModel.HUAWEI -> {
                (map as com.huawei.hms.maps.MapView).onStop()
            }
            MapManager.MapTypeModel.GOOGLE -> {
                (map as com.google.android.gms.maps.MapView).onStop()
            }
        }
    }

    fun onDestroy() {
        when (getMapTypeModel()) {
            MapManager.MapTypeModel.HUAWEI -> {
                (map as com.huawei.hms.maps.MapView).onDestroy()
            }
            MapManager.MapTypeModel.GOOGLE -> {
                (map as com.google.android.gms.maps.MapView).onDestroy()
            }
        }
    }

    fun onPause() {
        when (getMapTypeModel()) {
            MapManager.MapTypeModel.HUAWEI -> {
                (map as com.huawei.hms.maps.MapView).onPause()
            }
            MapManager.MapTypeModel.GOOGLE -> {
                (map as com.google.android.gms.maps.MapView).onPause()
            }
        }
    }

    fun onResume() {
        when (getMapTypeModel()) {
            MapManager.MapTypeModel.HUAWEI -> {
                (map as com.huawei.hms.maps.MapView).onResume()
            }
            MapManager.MapTypeModel.GOOGLE -> {
                (map as com.google.android.gms.maps.MapView).onResume()
            }
        }
    }

    fun onLowMemory() {
        when (getMapTypeModel()) {
            MapManager.MapTypeModel.HUAWEI -> {
                (map as com.huawei.hms.maps.MapView).onLowMemory()
            }
            MapManager.MapTypeModel.GOOGLE -> {
                (map as com.google.android.gms.maps.MapView).onLowMemory()
            }
        }
    }

    fun onSaveInstanceState(outState: Bundle) {
        when (getMapTypeModel()) {
            MapManager.MapTypeModel.HUAWEI -> {
                (map as com.huawei.hms.maps.MapView).onSaveInstanceState(outState)
            }
            MapManager.MapTypeModel.GOOGLE -> {
                (map as com.google.android.gms.maps.MapView).onSaveInstanceState(outState)
            }
        }
    }

    init {
        addView(map)
    }
}