package alirezat775.app.huaweiMap.mapino.wrapper

import androidx.annotation.NonNull
import androidx.annotation.Nullable

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class MarkerOptions {

    var huawei: com.huawei.hms.maps.model.MarkerOptions
    var google: com.google.android.gms.maps.model.MarkerOptions

    fun position(@NonNull value: LatLng): MarkerOptions {
        google = google.position(value.google)
        huawei = huawei.position(value.huawei)
        return this
    }

    fun zIndex(value: Float): MarkerOptions {
        google = google.zIndex(value)
        huawei = huawei.zIndex(value)
        return this
    }

    fun icon(@Nullable value: BitmapDescriptor): MarkerOptions {
        google = google.icon(value.google)
        huawei = huawei.icon(value.huawei)
        return this
    }

    fun anchor(x: Float, y: Float): MarkerOptions {
        google = google.anchor(x, y)
        huawei = huawei.anchorMarker(x, y)
        return this
    }

    fun infoWindowAnchor(x: Float, y: Float): MarkerOptions {
        google = google.infoWindowAnchor(x, y)
        huawei = huawei.infoWindowAnchor(x, y)
        return this
    }

    fun title(@Nullable value: String?): MarkerOptions {
        google = google.title(value)
        huawei = huawei.title(value)
        return this
    }

    fun snippet(@Nullable value: String?): MarkerOptions {
        google = google.snippet(value)
        huawei = huawei.snippet(value)
        return this
    }

    fun draggable(value: Boolean): MarkerOptions {
        google = google.draggable(value)
        huawei = huawei.draggable(value)
        return this
    }

    fun visible(value: Boolean): MarkerOptions {
        google = google.visible(value)
        huawei = huawei.visible(value)
        return this
    }

    fun flat(value: Boolean): MarkerOptions {
        google = google.flat(value)
        huawei = huawei.flat(value)
        return this
    }

    fun rotation(value: Float): MarkerOptions {
        google = google.rotation(value)
        huawei = huawei.rotation(value)
        return this
    }

    fun alpha(value: Float): MarkerOptions {
        google = google.alpha(value)
        huawei = huawei.alpha(value)
        return this
    }

    init {
        huawei = com.huawei.hms.maps.model.MarkerOptions()
        google = com.google.android.gms.maps.model.MarkerOptions()
    }
}