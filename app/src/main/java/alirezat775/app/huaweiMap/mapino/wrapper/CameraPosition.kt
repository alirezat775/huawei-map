package alirezat775.app.huaweiMap.mapino.wrapper

import android.view.ViewDebug.ExportedProperty


/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class CameraPosition(
    google: com.google.android.gms.maps.model.CameraPosition?,
    huawei: com.huawei.hms.maps.model.CameraPosition?
) {
    var google: com.google.android.gms.maps.model.CameraPosition?
    var huawei: com.huawei.hms.maps.model.CameraPosition?
    var tilt = 0f
    var zoom = 0f
    var target: LatLng? = null

    @ExportedProperty
    fun initTarget() {
        if (google != null) target = LatLng(google!!.target.latitude, google!!.target.longitude)
        if (huawei != null) target = LatLng(huawei!!.target.latitude, huawei!!.target.longitude)
    }

    fun initZoom() {
        if (google != null) zoom = google!!.zoom
        if (huawei != null) zoom = huawei!!.zoom
    }

    fun initTilt() {
        if (google != null) tilt = google!!.tilt
        if (huawei != null) tilt = huawei!!.tilt
    }

    class Builder {
        private var huawei: com.huawei.hms.maps.model.CameraPosition.Builder? = null
        private var google: com.google.android.gms.maps.model.CameraPosition.Builder? = null

        constructor() {
            google = com.google.android.gms.maps.model.CameraPosition.Builder()
            huawei = com.huawei.hms.maps.model.CameraPosition.Builder()
        }

        constructor(cameraPosition: CameraPosition) {
            if (cameraPosition.google != null) google =
                com.google.android.gms.maps.model.CameraPosition.Builder(cameraPosition.google)
            if (cameraPosition.huawei != null) huawei =
                com.huawei.hms.maps.model.CameraPosition.Builder(cameraPosition.huawei)
        }

        fun bearing(value: Float): Builder {
            google = google!!.bearing(value)
            huawei = huawei!!.bearing(value)
            return this
        }

        fun target(value: LatLng): Builder {
            google = google!!.target(value.google)
            huawei = huawei!!.target(value.huawei)
            return this
        }

        fun tilt(value: Float): Builder {
            google = google!!.tilt(value)
            huawei = huawei!!.tilt(value)
            return this
        }

        fun zoom(value: Float): Builder {
            google = google!!.zoom(value)
            huawei = huawei!!.zoom(value)
            return this
        }

        fun build(): CameraPosition {
            var pGoogle: com.google.android.gms.maps.model.CameraPosition? = null
            var pHuawei: com.huawei.hms.maps.model.CameraPosition? = null
            if (google != null) pGoogle = google!!.build()
            if (huawei != null) pHuawei = huawei!!.build()
            return CameraPosition(pGoogle, pHuawei)
        }
    }

    companion object {
        fun builder(): Builder {
            return Builder()
        }

        fun fromLatLngZoom(latLng: LatLng, zoom: Float): CameraPosition {
            return CameraPosition(
                com.google.android.gms.maps.model.CameraPosition.fromLatLngZoom(
                    latLng.google,
                    zoom
                ),
                com.huawei.hms.maps.model.CameraPosition.fromLatLngZoom(latLng.huawei, zoom)
            )
        }
    }

    init {
        this.google = google
        this.huawei = huawei
        initTarget()
        initTilt()
        initZoom()
    }
}