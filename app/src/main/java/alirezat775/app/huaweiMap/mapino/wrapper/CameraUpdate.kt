package alirezat775.app.huaweiMap.mapino.wrapper

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class CameraUpdate(
    var huawei: com.huawei.hms.maps.CameraUpdate? = null,
    var google: com.google.android.gms.maps.CameraUpdate? = null
) {
}