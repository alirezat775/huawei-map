package alirezat775.app.huaweiMap.mapino.wrapper

import android.graphics.Bitmap

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

object BitmapDescriptorFactory {
    const val HUE_RED = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_RED
    const val HUE_ORANGE = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_ORANGE
    const val HUE_YELLOW = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_YELLOW
    const val HUE_GREEN = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_GREEN
    const val HUE_CYAN = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_CYAN
    const val HUE_AZURE = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_AZURE
    const val HUE_BLUE = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_BLUE
    const val HUE_VIOLET = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_VIOLET
    const val HUE_MAGENTA = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_MAGENTA
    const val HUE_ROSE = com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_ROSE
    fun fromResource(id: Int): BitmapDescriptor {
        var google: com.google.android.gms.maps.model.BitmapDescriptor? = null
        var huawei: com.huawei.hms.maps.model.BitmapDescriptor? = null
        try {
            google = com.google.android.gms.maps.model.BitmapDescriptorFactory.fromResource(id)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            huawei = com.huawei.hms.maps.model.BitmapDescriptorFactory.fromResource(id)
        } catch (e: Exception) {
        }
        return BitmapDescriptor(huawei, google)
    }

    fun defaultMarker(hue: Float): BitmapDescriptor {
        var google: com.google.android.gms.maps.model.BitmapDescriptor? = null
        var huawei: com.huawei.hms.maps.model.BitmapDescriptor? = null
        try {
            google = com.google.android.gms.maps.model.BitmapDescriptorFactory.defaultMarker(hue)
        } catch (e: Exception) {
        }
        try {
            huawei = com.huawei.hms.maps.model.BitmapDescriptorFactory.defaultMarker(hue)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return BitmapDescriptor(huawei, google)
    }

    fun fromBitmap(bitmap: Bitmap?): BitmapDescriptor {
        var google: com.google.android.gms.maps.model.BitmapDescriptor? = null
        var huawei: com.huawei.hms.maps.model.BitmapDescriptor? = null
        try {
            google = com.google.android.gms.maps.model.BitmapDescriptorFactory.fromBitmap(bitmap)
        } catch (e: Exception) {
        }
        try {
            huawei = com.huawei.hms.maps.model.BitmapDescriptorFactory.fromBitmap(bitmap)
        } catch (e: Exception) {
        }
        return BitmapDescriptor(huawei, google)
    }
}