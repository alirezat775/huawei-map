package alirezat775.app.huaweiMap.mapino

import alirezat775.app.huaweiMap.mapino.wrapper.*
import android.annotation.SuppressLint
import android.location.Location
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.LocationSource

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class Mapino private constructor() {
    private var huaweiMap: HuaweiMap? = null
    private var googleMap: GoogleMap? = null

    val MAP_TYPE_NONE = GoogleMap.MAP_TYPE_NONE
    val MAP_TYPE_NORMAL = GoogleMap.MAP_TYPE_NORMAL
    val MAP_TYPE_SATELLITE = GoogleMap.MAP_TYPE_SATELLITE
    val MAP_TYPE_TERRAIN = GoogleMap.MAP_TYPE_TERRAIN
    val MAP_TYPE_HYBRID = GoogleMap.MAP_TYPE_HYBRID

    constructor(huaweiMap: HuaweiMap?) : this() {
        this.huaweiMap = huaweiMap
    }

    constructor(googleMap: GoogleMap?) : this() {
        this.googleMap = googleMap
    }

    interface OnCameraMoveStartedListener {
        fun onCameraMoveStarted(point: Int)

        companion object {
            const val REASON_GESTURE = 1
            const val REASON_API_ANIMATION = 2
            const val REASON_DEVELOPER_ANIMATION = 3
        }
    }

    interface OnMyLocationButtonClickListener {
        fun onMyLocationButtonClick(): Boolean
    }

    interface OnCameraIdleListener {
        fun onCameraIdle()
    }

    interface OnCameraMoveListener {
        fun onCameraMove()
    }

    interface OnCameraMoveCanceledListener {
        fun onCameraMoveCanceled()
    }

    interface CancelableCallback {
        fun onFinish()
        fun onCancel()
    }

    interface InfoWindowAdapter {
        fun getInfoWindow(marker: Marker?): View
        fun getInfoContents(marker: Marker?): View
    }

    interface OnMapClickListener {
        fun onMapClick(position: LatLng?)
    }

    interface OnMarkerClickListener {
        fun onMarkerClick(marker: Marker?): Boolean
    }

    interface OnMarkerDragListener {
        fun onMarkerDragStart(marker: Marker?)
        fun onMarkerDrag(marker: Marker?)
        fun onMarkerDragEnd(marker: Marker?)
    }

    interface OnMapLongClickListener {
        fun onMapLongClick(position: LatLng?)
    }

    interface OnInfoWindowClickListener {
        fun onInfoWindowClick(marker: Marker?)
    }

    interface OnInfoWindowCloseListener {
        fun onInfoWindowClose(marker: Marker?)
    }

    interface OnInfoWindowLongClickListener {
        fun onInfoWindowLongClick(marker: Marker?)
    }

    private fun isGoogle(): Boolean {
        return googleMap != null
    }

    private fun isHuawei(): Boolean {
        return huaweiMap != null
    }

    fun getMaxZoomLevel(): Float {
        return when {
            isHuawei() -> huaweiMap?.maxZoomLevel ?: 20f
            isGoogle() -> googleMap?.maxZoomLevel ?: 20f
            else -> 0f
        }
    }

    fun getMinZoomLevel(): Float {
        return when {
            isHuawei() -> huaweiMap?.maxZoomLevel ?: 1f
            isGoogle() -> googleMap?.maxZoomLevel ?: 1f
            else -> 0f
        }
    }

    fun setOnMyLocationButtonClickListener(listener: OnMyLocationButtonClickListener) {
        if (isHuawei()) {
            huaweiMap?.setOnMyLocationButtonClickListener { listener.onMyLocationButtonClick() }
        }
        if (isGoogle()) {
            googleMap?.setOnMyLocationButtonClickListener { listener.onMyLocationButtonClick() }
        }
    }


    fun setOnCameraMoveStartedListener(listener: OnCameraMoveStartedListener) {
        if (isHuawei()) {
            huaweiMap?.setOnCameraMoveStartedListener { value -> listener.onCameraMoveStarted(value) }
        }
        if (isGoogle()) {
            googleMap?.setOnCameraMoveStartedListener { value -> listener.onCameraMoveStarted(value) }
        }
    }

    fun setOnCameraIdleListener(listener: OnCameraIdleListener) {
        if (isHuawei()) {
            huaweiMap?.setOnCameraIdleListener { listener.onCameraIdle() }
        }
        if (isGoogle()) {
            googleMap?.setOnCameraIdleListener { listener.onCameraIdle() }
        }
    }

    fun setOnCameraMoveListener(listener: OnCameraMoveListener) {
        if (isHuawei()) {
            huaweiMap?.setOnCameraMoveListener { listener.onCameraMove() }
        }
        if (isGoogle()) {
            googleMap?.setOnCameraMoveListener { listener.onCameraMove() }
        }
    }

    fun setOnCameraMoveCanceledListener(listener: OnCameraMoveCanceledListener) {
        if (isHuawei()) {
            huaweiMap?.setOnCameraMoveCanceledListener { listener.onCameraMoveCanceled() }
        }
        if (isGoogle()) {
            googleMap?.setOnCameraMoveCanceledListener { listener.onCameraMoveCanceled() }
        }
    }

    fun setMapType(type: Int) {
        if (isHuawei()) huaweiMap?.mapType = type
        if (isGoogle()) googleMap?.mapType = type
    }

    fun getMapType(): Int {
        return when {
            isHuawei() -> huaweiMap?.mapType ?: 1
            isGoogle() -> googleMap?.mapType ?: 1
            else -> -1
        }
    }

    @SuppressLint("MissingPermission")
    fun setMyLocationEnabled(value: Boolean) {
        if (isHuawei()) huaweiMap?.isMyLocationEnabled = value
        if (isGoogle()) googleMap?.isMyLocationEnabled = value
    }

    fun isMyLocationEnabled(): Boolean {
        return when {
            isHuawei() -> huaweiMap?.isMyLocationEnabled ?: false
            isGoogle() -> googleMap?.isMyLocationEnabled ?: false
            else -> false
        }
    }

    fun getCameraPosition(): CameraPosition? {
        var cameraPosition: CameraPosition? = null
        if (googleMap != null) cameraPosition = CameraPosition(googleMap?.cameraPosition, null)
        if (huaweiMap != null) cameraPosition = CameraPosition(null, huaweiMap?.cameraPosition)
        return cameraPosition
    }

    fun getUiSettings(): UiSettings? {
        if (isGoogle()) return UiSettings(googleMap)
        return if (isHuawei()) UiSettings(huaweiMap) else null
    }

    fun setContentDescription(value: String?) {
        if (isHuawei()) huaweiMap?.setContentDescription(value)
        if (isGoogle()) googleMap?.setContentDescription(value)
    }

    fun setMaxZoomPreference(value: Float) {
        if (isHuawei()) huaweiMap?.setMaxZoomPreference(value)
        if (isGoogle()) googleMap?.setMaxZoomPreference(value)
    }

    fun setMinZoomPreference(value: Float) {
        if (isHuawei()) huaweiMap?.setMinZoomPreference(value)
        if (isGoogle()) googleMap?.setMinZoomPreference(value)
    }

    fun resetMinMaxZoomPreference() {
        if (isHuawei()) huaweiMap?.resetMinMaxZoomPreference()
        if (isGoogle()) googleMap?.resetMinMaxZoomPreference()
    }

    fun clear() {
        if (isHuawei()) huaweiMap?.clear()
        if (isGoogle()) googleMap?.clear()
    }

    fun animateCamera(value: CameraUpdate) {
        if (isGoogle()) googleMap?.animateCamera(value.google)
        if (isHuawei()) huaweiMap?.animateCamera(value.huawei)
    }

    fun moveCamera(value: CameraUpdate) {
        if (isGoogle()) googleMap?.moveCamera(value.google)
        if (isHuawei()) huaweiMap?.moveCamera(value.huawei)
    }

    fun animateCamera(value: CameraUpdate, callback: CancelableCallback?) {
        if (isGoogle()) {
            googleMap?.animateCamera(value.google, object : GoogleMap.CancelableCallback {
                override fun onFinish() {
                    callback?.onFinish()
                }

                override fun onCancel() {
                    callback?.onCancel()
                }
            })
        }
        if (isHuawei()) {
            huaweiMap?.animateCamera(value.huawei, object : HuaweiMap.CancelableCallback {
                override fun onFinish() {
                    callback?.onFinish()
                }

                override fun onCancel() {
                    callback?.onCancel()
                }
            })
        }
    }

    fun animateCamera(cameraUpdate: CameraUpdate, value: Int, callback: CancelableCallback?) {
        if (isGoogle()) {
            googleMap?.animateCamera(
                cameraUpdate.google,
                value,
                object : GoogleMap.CancelableCallback {
                    override fun onFinish() {
                        callback?.onFinish()
                    }

                    override fun onCancel() {
                        callback?.onCancel()
                    }
                })
        }
        if (isHuawei()) {
            huaweiMap?.animateCamera(
                cameraUpdate.huawei,
                value,
                object : HuaweiMap.CancelableCallback {
                    override fun onFinish() {
                        callback?.onFinish()
                    }

                    override fun onCancel() {
                        callback?.onCancel()
                    }
                })
        }
    }

    fun setInfoWindowAdapter(adapter: InfoWindowAdapter) {
        if (isGoogle()) {
            googleMap?.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
                override fun getInfoWindow(marker: com.google.android.gms.maps.model.Marker): View {
                    return adapter.getInfoWindow(Marker(marker, null))
                }

                override fun getInfoContents(marker: com.google.android.gms.maps.model.Marker): View {
                    return adapter.getInfoContents(Marker(marker, null))
                }
            })
        }
        if (isHuawei()) {
            huaweiMap?.setInfoWindowAdapter(object : HuaweiMap.InfoWindowAdapter {
                override fun getInfoContents(marker: com.huawei.hms.maps.model.Marker): View {
                    return adapter.getInfoContents(Marker(null, marker))
                }

                override fun getInfoWindow(marker: com.huawei.hms.maps.model.Marker): View {
                    return adapter.getInfoWindow(Marker(null, marker))
                }
            })
        }
    }

    fun addMarker(markerOptions: MarkerOptions): Marker? {
        var marker: Marker? = null
        if (isGoogle()) {
            marker = Marker(googleMap?.addMarker(markerOptions.google), null)
        }
        if (isHuawei()) {
            marker = Marker(null, huaweiMap?.addMarker(markerOptions.huawei))
        }
        return marker
    }

    fun setOnMarkerClickListener(listener: OnMarkerClickListener) {
        if (isGoogle()) googleMap?.setOnMarkerClickListener { marker ->
            listener.onMarkerClick(Marker(marker, null))
        }
        if (isHuawei()) huaweiMap?.setOnMarkerClickListener { marker ->
            listener.onMarkerClick(Marker(null, marker))
        }
    }

    fun setOnMarkerDragListener(listener: OnMarkerDragListener) {
        if (isGoogle()) googleMap?.setOnMarkerDragListener(object :
            GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(marker: com.google.android.gms.maps.model.Marker) {
                listener.onMarkerDragStart(Marker(marker, null))
            }

            override fun onMarkerDrag(marker: com.google.android.gms.maps.model.Marker) {
                listener.onMarkerDrag(Marker(marker, null))
            }

            override fun onMarkerDragEnd(marker: com.google.android.gms.maps.model.Marker) {
                listener.onMarkerDragEnd(Marker(marker, null))
            }
        })
        if (isHuawei()) huaweiMap?.setOnMarkerDragListener(object :
            HuaweiMap.OnMarkerDragListener {
            override fun onMarkerDragStart(marker: com.huawei.hms.maps.model.Marker) {
                listener.onMarkerDragStart(Marker(null, marker))
            }

            override fun onMarkerDrag(marker: com.huawei.hms.maps.model.Marker) {
                listener.onMarkerDragStart(Marker(null, marker))
            }

            override fun onMarkerDragEnd(marker: com.huawei.hms.maps.model.Marker) {
                listener.onMarkerDragStart(Marker(null, marker))
            }
        })
    }

    fun setOnMapClickListener(listener: OnMapClickListener) {
        if (isGoogle()) googleMap?.setOnMapClickListener { latLng ->
            listener.onMapClick(LatLng(latLng.latitude, latLng.longitude))
        }
        if (isHuawei()) huaweiMap?.setOnMapClickListener { latLng ->
            listener.onMapClick(LatLng(latLng.latitude, latLng.longitude))
        }
    }

    fun setOnMapLongClickListener(listener: OnMapLongClickListener) {
        if (isGoogle()) googleMap?.setOnMapLongClickListener { latLng ->
            listener.onMapLongClick(
                LatLng(
                    latLng.latitude,
                    latLng.longitude
                )
            )
        }
        if (isHuawei()) huaweiMap?.setOnMapLongClickListener { latLng ->
            listener.onMapLongClick(
                LatLng(latLng.latitude, latLng.longitude)
            )
        }
    }

    fun stopAnimation() {
        if (isHuawei()) huaweiMap?.stopAnimation()
        if (isGoogle()) googleMap?.stopAnimation()
    }

    fun setOnInfoWindowClickListener(listener: OnInfoWindowClickListener) {
        if (isGoogle()) googleMap?.setOnInfoWindowClickListener { marker ->
            listener.onInfoWindowClick(
                Marker(marker, null)
            )
        }
        if (isHuawei()) huaweiMap?.setOnInfoWindowClickListener { marker ->
            listener.onInfoWindowClick(
                Marker(null, marker)
            )
        }
    }

    fun setOnInfoWindowCloseListener(listener: OnInfoWindowCloseListener) {
        if (isGoogle()) googleMap?.setOnInfoWindowCloseListener { marker ->
            listener.onInfoWindowClose(
                Marker(marker, null)
            )
        }
        if (isHuawei()) huaweiMap?.setOnInfoWindowCloseListener { marker ->
            listener.onInfoWindowClose(
                Marker(null, marker)
            )
        }
    }

    fun setOnInfoWindowLongClickListener(listener: OnInfoWindowLongClickListener) {
        if (isGoogle()) googleMap?.setOnInfoWindowLongClickListener { marker ->
            listener.onInfoWindowLongClick(
                Marker(marker, null)
            )
        }
        if (isHuawei()) huaweiMap?.setOnInfoWindowLongClickListener { marker ->
            listener.onInfoWindowLongClick(
                Marker(null, marker)
            )
        }
    }

    fun setLocationSource(locationSource: LocationSource) {
        if (isGoogle()) {
            val google: com.google.android.gms.maps.LocationSource =
                object : com.google.android.gms.maps.LocationSource {
                    override fun activate(onLocationChangedListener: com.google.android.gms.maps.LocationSource.OnLocationChangedListener) {
                        val listener: LocationSource.OnLocationChangedListener =
                            object :
                                com.google.android.gms.maps.LocationSource.OnLocationChangedListener,
                                LocationSource.OnLocationChangedListener {
                                override fun onLocationChanged(location: Location?) {
                                    onLocationChangedListener.onLocationChanged(location)
                                }
                            }
                        locationSource.activate(listener)
                    }

                    override fun deactivate() {
                        locationSource.deactivate()
                    }
                }
            googleMap?.setLocationSource(google)
        } else if (isHuawei()) {
            val huawei: LocationSource = object : LocationSource {
                override fun activate(onLocationChangedListener: LocationSource.OnLocationChangedListener) {
                    val listener: LocationSource.OnLocationChangedListener =
                        LocationSource.OnLocationChangedListener { location ->
                            onLocationChangedListener.onLocationChanged(
                                location
                            )
                        }
                    locationSource.activate(listener)
                }

                override fun deactivate() {
                    locationSource.deactivate()
                }
            }
            huaweiMap?.setLocationSource(huawei)
        }
    }
}