package alirezat775.app.huaweiMap.mapino

import android.os.Bundle

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class MapManager(
    private val mapView: MapView
) {

    enum class MapTypeModel(val type: Int) {
        HUAWEI(0),
        GOOGLE(1)
    }

    fun getMapType(): MapTypeModel {
        return mapView.getMapTypeModel()
    }

    private fun isGoogle(): Boolean {
        return getMapType() == MapTypeModel.GOOGLE
    }

    private fun isHuawei(): Boolean {
        return getMapType() == MapTypeModel.HUAWEI
    }

    fun onCreateMap(bundle: Bundle?) {
        if (isHuawei()) {
            (mapView.getMap() as com.huawei.hms.maps.MapView).onCreate(bundle)
        }
        if (isGoogle()) {
            (mapView.getMap() as com.google.android.gms.maps.MapView).onCreate(bundle)
        }
    }

    fun getMapAsync(listener: MapAsyncListener) {
        if (isHuawei()) {
            (mapView.getMap() as com.huawei.hms.maps.MapView).getMapAsync {
                listener.onMapReady(Mapino(it))
            }
        }
        if (isGoogle()) {
            (mapView.getMap() as com.google.android.gms.maps.MapView).getMapAsync {
                listener.onMapReady(Mapino(it))
            }
        }
    }
}