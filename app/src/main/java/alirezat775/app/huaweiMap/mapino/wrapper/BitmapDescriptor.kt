package alirezat775.app.huaweiMap.mapino.wrapper

/**
 * Author:  Alireza Tizfahm Fard
 * Date:    9/3/20
 * Email:   alirezat775@gmail.com
 */

class BitmapDescriptor(
    var huawei: com.huawei.hms.maps.model.BitmapDescriptor? = null,
    var google: com.google.android.gms.maps.model.BitmapDescriptor? = null
) {
}