# Huawei Map App!
In this app we will work with huawei map-kit and location-kit, firstly we for having resuable code and reduce refactor cost we have been created wrapper on the huawei SDKs and google SDKs,
App start with **MapActivity**, from this activity user can choose Huawei  map and Google map, we have tried to have same interface in any map SDK, that's mean both of them have same implementaion with **Mapino** , in this class we try to get location permission from user.


### Mapino
we call wrapper on the map as **mapino**
#### mapino how to works?
firstly create object instance of **MapView** in xml layout or any view, next step is must to pass mapTypeModel (Huawei, Google) for initializing map.
The **MapManager** class try to init other classes with type have been passed to mapView, For staring using mapino as basic mode, just create new instance from  **MapManager**, and call **onCreateMap()** and **getMapAsync()**.
declare new interface for getting is map ready, map ready return instance of mapino, mapino is wrapper on map SDKs, in this scope have been tried to implement esensial function, every function in mapino have same interface with map SDKs, such as `addMarker`, `setOnMapLongClickListener`, `setOnMapClickListener` and ... .
 After map was ready you can use any method for working with map. 

### Locationino
we call wrapper on the location as **locationino**
Like the mapino, **Locationino** is wrapper on location services, (Huawei, Google), For working with the class, create new instance and pass `FusedLocationProviderClient` as constructor paramter, and call `getLastLocation` with a listener for getting location changes.


### App Senario 
Note: google and huawei have same implementaion in this app.
For example we gonna to explain Huawei.
Firstly bind map lifecycle with activity lifecycle, next step is initilize map, this is in onCreate function, pass bundle and listener to map,
#### First senario: showing map and user can find current location
Default we set map on Tehran location, if user pass location access to the app the find my current. location button is visible for user, if user click on the button map animate camera to user current location,
#### Second senario: add marker any point of user has been clicked
We assign a marker to the map with **star** icon, add new listener for getting click on the map, in result, we create new instance from markerOption and pass suitable value, such as icon, and in this step we try to convert geocode to human-readable-address, and adding to marker title.
if user click on the map marker will be show, and if user click on the marker user get address data in pop-up view above of marker.
#### Third senario: pick location from any point of map with fixed marker
in this senario we adding pin imageView top of the map layer in xml, and assign a textView for showing pin address result.
After activity is ready to work (lifecycle steps), in oncreate we add listener for getting click on the pin imageView, we user click on the pin we try to get lat, long from camera position and pass they to location manager for transform to human-readable-address, and after getting address fill the pin address textView with value.